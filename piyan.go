package main

import (
    "bufio"
    "fmt"
    "os"
    "strings"
)

func main() {
    var text string
    baca := bufio.NewReader(os.Stdin)
    fmt.Print("Input n:")
    in1, _ := baca.ReadString('\n')
    text = strings.TrimSuffix(in1, "\r\n")
    text = strings.ToLower(text)
    text = strings.ReplaceAll(text, " ", "")

    arrText := strings.Split(text, "")
    fmt.Println("Vokal:")
    Vocal(arrText)
    fmt.Print("\nKonsonan:\n")
    Konsonan(arrText)

}

func Vocal(data []string) {
    var vokal []string
    str:="aiueo"
    for i := 0; i < len(data); i++ {
        if strings.Contains(str,data[i]) {
            vokal = append(vokal, data[i])
        }
    }

    for i := 0; i < len(vokal); i++ {
        for j := 0; j < len(vokal)-1; j++ {
            if vokal[j] == vokal[j+1] {
                break
            } else {
                temp := vokal[j+1]
                vokal[j+1] = vokal[j]
                vokal[j] = temp
            }
        }
    }

    for i := 0; i < len(vokal); i++ {
        fmt.Print(vokal[i])
    }
}

func Konsonan(data []string) {
    var konsonan []string

    str := "qwrtypsdfghjklzxcvbnm"
    for i := 0; i < len(data); i++ {
        if strings.Contains(str,data[i]) {
            konsonan = append(konsonan, data[i])
        }
    }
    for i := 0; i < len(konsonan); i++ {
        for j := 0; j < len(konsonan)-1; j++ {
            if konsonan[j] == konsonan[j+1] {
                break
            } else {
                temp := konsonan[j+1]
                konsonan[j+1] = konsonan[j]
                konsonan[j] = temp
            }
        }
    }

    for i := 0; i < len(konsonan); i++ {
        fmt.Print(konsonan[i])
    }
}