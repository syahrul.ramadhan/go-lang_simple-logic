package main

import (
	"fmt"
	"math"
)

func main() {
	var nilai = 0.0023124
	fmt.Println(toFixed(nilai,3))
	fmt.Println(math.Round(nilai))
	fmt.Println(roundFloat(nilai,3))
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}

func round(num float64) int {
    return int(num + math.Copysign(0.5, num))
}

func roundFloat(val float64, precision uint) float64 {
    ratio := math.Pow(10, float64(precision))
    return math.Round(val*ratio) / ratio
}