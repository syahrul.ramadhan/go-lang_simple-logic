package main

import "fmt"

func main() {
	//program sorting nilai secara ascending
	//deklarasi variabel
	var nilai = []int{5, 6, 7, 1, 2, 3, 9}
	var tampung int
	//cetak nilai awal
	fmt.Println("nilai awal:")
	for i := 0; i < len(nilai); i++ {
		fmt.Print(nilai[i], " ")
	}
	//proses dalam melakukan sorting dengan cara membandingkan
	//antara nilai satu dengan nilai sebelahnya
	for i := len(nilai); i > 0; i-- {
		for j := 1; j < i; j++ {
			if nilai[j-1] > nilai[j] {
				tampung = nilai[j]
				nilai[j] = nilai[j-1]
				nilai[j-1] = tampung
			}
		}
	}
	//cetak hasil
	fmt.Println()
	fmt.Println("nilai akhir:")
	for i := 0; i < len(nilai); i++ {
		fmt.Print(nilai[i], " ")
	}

}
