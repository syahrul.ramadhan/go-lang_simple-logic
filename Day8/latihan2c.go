package main

import "fmt"

func main() {
	//program menginputkan nilai array sesuai dengan panjang array yang di inputkan
	//deklarasi variabel
	var input int
	//inputkan panjang array
	fmt.Print("masukkan panjang array ?")
	fmt.Scanln(&input)
	nilai := make([]int, input)
	//inputkan nilai array
	for i := 0; i < input; i++ {
		fmt.Print("Array posisi ", i+1, " ? ")
		fmt.Scanln(&nilai[i])
	}
	//cetak hasil array
	for i := 0; i < len(nilai); i++ {
		fmt.Print(nilai[i], " ")
	}

}
