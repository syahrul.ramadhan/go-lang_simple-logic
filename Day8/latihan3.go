package main

import "fmt"

func main() {
	//program menentukan nilai yang diinputkan bernilai genap atau ganjil
	//deklarasi variabel
	var nilai int
	//input panjang deretnya
	fmt.Print("panjang ? ")
	fmt.Scanln(&nilai)
	var deret = make([]int, nilai)
	//input nilai sesuai dengan panjang deret
	for i := 0; i < nilai; i++ {
		fmt.Print("n", i+1, ": ")
		fmt.Scanln(&deret[i])

	}
	//proses menentukan nilai tersebut ganjil atau genap
	for i := 0; i < len(deret); i++ {
		if deret[i]%2 == 0 {
			fmt.Print(deret[i], ": Genap", "\n")
			//tampung = append(tampung, deret[i])
		} else {
			fmt.Print(deret[i], ": Ganjil", "\n")
			//tampung2 = append(tampung2, deret[i])
		}

	}
	fmt.Println()
	fmt.Print("Genap :")
	for i := 0; i < len(deret); i++ {
		if deret[i]%2 == 0 {
			fmt.Print(deret[i], " ")
		}
	}
	fmt.Println()
	fmt.Print("Ganjil :")
	for i := 0; i < len(deret); i++ {
		if deret[i]%2 != 0 {
			fmt.Print(deret[i], " ")
		}
	}

}
