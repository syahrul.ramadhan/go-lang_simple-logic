package main

import "fmt"

func main() {
	//program menghitung nilai maximal atau minimal
	//deklarasi variabel
	var nilai = []int{5, 6, 7, 1, 2, 3, 9}
	var max = nilai[0]
	var min = nilai[0]
	//proses mencari nilai maximal
	for i := 0; i < len(nilai); i++ {
		if nilai[i] > max {
			max = nilai[i]
		}
	}
	//proses mencari nilai minimal
	for i := 0; i < len(nilai); i++ {
		if nilai[i] < min {
			min = nilai[i]
		}
	}

	fmt.Println("nilai tertinggi :", max)
	fmt.Println("nilai terrendah :", min)
}
