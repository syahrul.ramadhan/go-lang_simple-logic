package main

import "fmt"

func main() {
	//program menambahkan nilai dengan panjang di tentukan dan
	//bernilai ganjil dan genap
	//deklarasi variabel
	var panjang int
	//menginputkan nilai panjang deret
	fmt.Print("panjang deret : ")
	fmt.Scanln(&panjang)

	var ganjil = make([]int, panjang)
	var genap = make([]int, panjang)
	var penjumlah = 1
	var penjumlah2 = 2
	var jumlah1 int
	var jumlah2 int
	//proses memasukkan data ganjil ke array serta
	//total nilai array
	for i := 0; i < len(ganjil); i++ {
		ganjil[i] = penjumlah
		penjumlah += 2
	}
	//proses memasukkan data genap ke array serta
	//total nilai array
	for i := 0; i < len(genap); i++ {
		genap[i] = penjumlah2
		penjumlah2 += 2
	}
	//cetak nilai ganjil
	fmt.Print("Ganjil :")
	for i := 0; i < len(ganjil); i++ {
		if i < len(ganjil)-1 {
			fmt.Print(ganjil[i], ",", " ")
		} else {
			fmt.Print(ganjil[i])
		}
		jumlah1 = jumlah1 + ganjil[i]
	}
	fmt.Println()
	fmt.Println("jumlah :", jumlah1)
	fmt.Println()
	//cetak nilai genap
	fmt.Print("Genap :")
	for i := 0; i < len(genap); i++ {
		if i < len(genap)-1 {
			fmt.Print(genap[i], ",", " ")
		} else {
			fmt.Print(genap[i])
		}
		jumlah2 += genap[i]
	}
	fmt.Println()
	fmt.Println("jumlah :", jumlah2)
}
