package main

import "fmt"

func main() {
	//program inputkan array string jenis-jenis ikan
	//deklarasi variabel
	var panjang int
	//input panjang array
	fmt.Print("tentukan banyak ikan :")
	fmt.Scanln(&panjang)
	//proses memasukkan data
	var jenis = make([]string, panjang)
	for i := 0; i < len(jenis); i++ {
		fmt.Print("Ikan ke ", i+1, " : ")
		fmt.Scanln(&jenis[i])
	}

	//cetak hasil
	fmt.Println()
	fmt.Println("ikan yang diinputkan :")
	fmt.Print("Macam ikan : ")
	for i := 0; i < len(jenis); i++ {
		fmt.Print(jenis[i], " ")
	}
}
