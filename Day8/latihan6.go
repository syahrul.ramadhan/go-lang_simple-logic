package main

import "fmt"

func main() {

	//program mencari nilai yang ada didalam array
	//cara saya
	//deklarasi variabel
	var nilai = []int{5, 6, 7, 1, 2, 3, 9}

	var bilangan int
	var hasil string
	fmt.Print("cari nilai : ")
	fmt.Scanln(&bilangan)
	//proses mengolah array untuk mencari deret
	for i := 0; i < len(nilai); i++ {
		if nilai[i] == bilangan {
			hasil = " ada dalam deret"
			break
		} else {
			hasil = " tidak ada dalam deret"
		}
	}
	//cetak hasil yang telah di proses
	fmt.Print("nilai ", bilangan, hasil)
	// if hasil == true {
	// 	fmt.Print("nilai ", bilangan, " ada dalam deret")
	// }
	// if hasil == false {
	// 	fmt.Print("nilai ", bilangan, " tidak ada dalam deret")
	// }

	//cara trainer
	var fCari bool
	for i := 0; i < len(nilai); i++ {
		if nilai[i] == bilangan {
			fCari = true
			break
		}
	}
	fmt.Println()
	if fCari == true {
		fmt.Print("nilai ", bilangan, " ada dalam deret")
	} else {
		fmt.Print("nilai ", bilangan, " tidak ada dalam deret")
	}
}
