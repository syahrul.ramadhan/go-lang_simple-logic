package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	//program mencetak bintang sesuai dengan jumlah kalimat yang dimasukkan
	var nama string
	var bintang string
	baca := bufio.NewReader(os.Stdin)
	fmt.Print("inputkan nama panjang :")
	in1, _ := baca.ReadString('\n')
	nama = strings.TrimSuffix(in1, "\r\n")
	for i := 0; i < len(nama); i++ {
		bintang += "*"
	}
	nama = strings.ToUpper(nama)
	fmt.Print(bintang, nama, bintang)
}
