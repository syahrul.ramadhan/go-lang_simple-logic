package main

import "fmt"

func main() {
	//cara satu deklarasi array 1 dimensi
	var nama [4]string //default element yaitu kosong
	var pilih [2]bool  //default element yaitu false
	//cara dua deklarasi array
	var nilai = [...]int{1, 2, 3, 4, 5, 6} //default element yaitu kosong
	fmt.Print(nilai[0], "\n")              //mencetak sesuai dengan index yg dipanggil

	//isi array
	nama[0] = "syahrul"
	fmt.Println(nama)  //mencetak semua data array yg ada di variable
	fmt.Println(pilih) //mencetak semua data array yg ada di variable
}
