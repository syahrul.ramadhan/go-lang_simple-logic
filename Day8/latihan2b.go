package main

import "fmt"

func main() {
	//program sederhana menginputkan nilai array
	var nilai [5]int
	//var input int
	var urut = 1

	for i := 0; i < len(nilai); i++ {
		fmt.Print("Array posisi ", i+1, "? ")
		fmt.Scanln(&nilai[i])
		//nilai[i] = input
		urut++
	}

	for i := 0; i < len(nilai); i++ {
		fmt.Print(nilai[i], " ")
	}
}
