package main

import "fmt"

func main() {
	var warna string
	var rambu = ""
	fmt.Print("Lampu ? ")
	fmt.Scanln(&warna)

	switch warna {
	case "merah":
		rambu = "berhenti"
	case "hijau":
		rambu = "jalan"
	case "kuning":
		rambu = "bersiap"
	default:
		fmt.Println("Rusak")
	}

	fmt.Println("lampu", warna, "harus", rambu)
}
