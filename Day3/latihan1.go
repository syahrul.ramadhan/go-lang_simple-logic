package main

import "fmt"

func main() {
	var nilai int
	fmt.Print("masukkan nilai :")
	fmt.Scanln(&nilai)

	// fmt.Println("Hasil :")
	// switch nilai {
	// case 1:
	// 	fallthrough
	// case 2:
	// 	fallthrough
	// case 3:
	// 	fmt.Println("Gagal")
	// case 4:
	// 	fallthrough
	// case 5:
	// 	fallthrough
	// case 6:
	// 	fallthrough
	// case 7:
	// 	fmt.Println("Lulus")
	// default:
	// 	fmt.Println("Nilai Melebihi batas")
	// }
	switch {
	case nilai >= 11:
		fmt.Println("Nilai Melebihi Batas")
	case nilai <= 3 && nilai >= 1:
		fmt.Println("Anda Gagal")
	case nilai >= 4 && nilai <= 6:
		fmt.Println("Remedial")
	}
	//	kondisi()
}

func kondisi() {
	var nilai int
	fmt.Print("Masukkan nilai :")
	fmt.Scanln(&nilai)
	if nilai >= 11 {
		fmt.Println("salah nilai")
	} else {

		if nilai == 1 || nilai <= 3 {
			fmt.Println("Anda Gagal dengan nilai :", nilai)
		} else if nilai <= 5 {
			fmt.Println("Anda Remedial dengan nilai :", nilai)
		} else {
			fmt.Println("Anda Lulus dengan nilai :", nilai)

		}
	}
}
