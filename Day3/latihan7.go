package main

import "fmt"

func main() {
	var money int
	var hasil string
	fmt.Print("Masukkan uang ryan :")
	fmt.Scanln(&money)

	if money >= 750_000 {
		hasil = "Jas"
		money -= 750_000
	} else if money >= 350_000 {
		hasil = "Celana Panjang"
		money -= 350_000
	} else if money >= 225_000 {
		hasil = "Kemeja"
		money -= 225_000
	} else if money >= 100_000 {
		hasil = "Kaos"
		money -= 100_000
	} else if money >= 50_000 {
		hasil = "Celana Pendek"
		money -= 50_000
	}
	fmt.Println("Ryan Beli", hasil)
	fmt.Println("Ryan uang sisa :", money)
}
