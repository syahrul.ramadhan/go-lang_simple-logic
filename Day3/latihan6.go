package main

import (
	"fmt"
	m "math/rand"
	"time"
)

func main() {
	var min = 1
	var max = 10
	var acak int
	m.Seed(time.Now().UnixNano())
	acak = m.Intn(max-min) + min

	if acak%2 == 0 {
		fmt.Println("nilai acak :", acak)
		fmt.Println("merupakan bilangan genap")
	} else {
		fmt.Println("nilai acak :", acak)
		fmt.Println("merupakan bilangan ganjil")
	}
}
