package main

import "fmt"

func main() {
	//deklarasi
	var musim, hari, mood string
	//input
	fmt.Print("Masukkan musim :")
	fmt.Scanln(&musim)
	fmt.Print("Masukkan hari :")
	fmt.Scanln(&hari)
	//proses
	if musim == "hujan" && hari == "libur" {
		mood = "rehat di rumah"
	} else if musim == "hujan" && hari == "kerja" {
		mood = "pasrah"
	} else if musim == "panas" && hari == "libur" {
		mood = "hangout bareng teman"
	} else if musim == "panas" && hari == "kerja" {
		mood = "semangat 45"
	} else {
		mood = "gabut"
	}
	//output
	fmt.Println("musim ", musim, "dan hari", hari, "maka syahrul", mood, "saja")
}
