package main

import "fmt"

func main() {
	nilai := 10
	fmt.Println("Mulai")
	//if
	if nilai == 10 {
		fmt.Println("lulus")
	}
	//if else
	if nilai == 9 {
		fmt.Println("lulus") // true statement
	} else {
		fmt.Println("gagal") // false statement
	}
	fmt.Println("Selesai")
}
