package main

import (
	"fmt"
)

func main() {
	lampu := "merah"

	fmt.Println("Status lampu :", lampu)

	switch lampu {
	case "merah":
		fmt.Println("Berhenti")
	case "kuning":
		fmt.Println("Siap-Siap")
	case "hijau":
		fmt.Println("Jalan")
	default:
		fmt.Println("kecelakaan")
	}
}
