package main

import "fmt"

func main() {
	var bPel, bNov, bSkrip, hari, denda int
	fmt.Print("jumlah buku pelajaran :")
	fmt.Scanln(&bPel)
	fmt.Print("jumlah buku novel :")
	fmt.Scanln(&bNov)
	fmt.Print("jumlah buku skripsi :")
	fmt.Scanln(&bSkrip)
	fmt.Print("Lama Pinjam :")
	fmt.Scanln(&hari)

	if hari > 10 {
		hari -= 10
		denda = ((bPel * 2_000) + (bNov * 5_000) + (bSkrip * 10_000)) * hari
		fmt.Println("Total denda :", denda)
	} else {
		fmt.Println("Total denda gratis")
	}

}
