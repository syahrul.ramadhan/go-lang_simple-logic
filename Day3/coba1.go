package main

import (
	"fmt"
	ma "math/rand"
	"time"
)

func main() {
	min := 1
	max := 10
	var random, input int

	fmt.Print("Masukkan angka mu :")
	fmt.Scanln(&input)
	if input > max && input < min {
		fmt.Println("nilai melebihi batas")
	} else {
		ma.Seed(time.Now().UnixNano())
		random = ma.Intn(max-min) + min

		fmt.Println("nilai musuh :", random)

		if random == input {
			fmt.Println("Nilai Seri")
		} else if random < input {
			fmt.Println("Anda menang")
		} else if random > input {
			fmt.Println("Anda kalah")
		}
	}
}
