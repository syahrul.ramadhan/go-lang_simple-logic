package main // harus ada package setiap satu file go

import "fmt"

func main() { // harus ada func main setiap satu file go

	//contoh literal huruf atau kata
	fmt.Println("syahrul ramadhan !!!") //cetak kata dengan pindah baris setelah kata

	fmt.Println() //digunakan untuk pindah barus

	fmt.Print("Cibitung")

	//contoh literal angka atau kumpulan angka
	fmt.Println(12345)

	//join literal string and number
	fmt.Println("Umur :", 24)

}
