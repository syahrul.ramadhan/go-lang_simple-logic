package main

import "fmt"

func main() {
	//deklarasi variable
	var p, l, t int

	fmt.Println("Hitung Volume Balok")
	//input panjang
	fmt.Print("input panjang : ")
	fmt.Scanln(&p)
	//input lebar
	fmt.Print("input Lebar : ")
	fmt.Scanln(&l)
	//input tinggi
	fmt.Print("input Tinggi : ")
	fmt.Scanln(&t)

	fmt.Println("Volume", p, "*", l, "*", t, ": ", p*l*t)
}
