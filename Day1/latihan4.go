package main

import "fmt"

func main() {
	p, l, t := 2, 3, 5

	fmt.Println("P=", p)
	fmt.Println("L=", l)
	fmt.Println("T=", t)

	fmt.Println("Volume", p, "*", l, "*", t, "*", "=", p*l*t)
}
