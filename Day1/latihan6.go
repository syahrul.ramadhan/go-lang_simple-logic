package main

import "fmt"

func main() {
	a, b := 5, 2

	fmt.Println("nilai awal")
	fmt.Println("A =", a)
	fmt.Println("B =", b)
	fmt.Println("nilai telah ditukar")
	c := a
	a = b
	b = c
	fmt.Println("A =", a)
	fmt.Println("B =", b)

}
