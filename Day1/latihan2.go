package main

import "fmt"

func main() {

	var identitas = 12345          // variable dengan angka atau int tanpa tipe data
	nama := "Xsis"                 // variable dengan huruf atau string tanpa var dan tipe data
	var alamat string = "jl.Bogor" // variable string dengan tipe data dan var
	umur := 22
	ttl := "depok,03/12/1999"
	contoh1, contoh2 := "satu", 2 // multiple variable dengan string atau int

	fmt.Println(contoh1, contoh2)

	fmt.Println("Identitas 	:", identitas)
	fmt.Println("Nama 		:", nama)
	fmt.Println("Alamat 		:", alamat)
	fmt.Println("Umur 		:", umur)
	fmt.Println("Ttl 		:", ttl)
}
