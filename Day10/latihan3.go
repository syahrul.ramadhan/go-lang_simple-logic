package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {

	//program palindrome
	//deklarasi varibel
	var sNama string
	baca := bufio.NewReader(os.Stdin)
	var aAkhir []string
	var sAkhir string
	var hasil string
	//input kalimat palindrome atau bukan
	fmt.Print("nama :")
	in1, _ := baca.ReadString('\n')
	sNama = in1
	//hilangkan \n
	sNama = strings.TrimSuffix(sNama, "\r\n")
	//ubah semua string dalam variabel menjadi huruf kecil
	sNama = strings.ToLower(sNama)
	//cacah string ke dalam array
	aAWal := strings.Split(sNama, "")
	//membalikan kata yang ada dalam array aAwal
	for i := len(aAWal) - 1; i >= 0; i-- {
		aAkhir = append(aAkhir, aAWal[i])
	}
	//proses pengecekan palindrom atau bukan
	for i := 0; i < len(aAWal); i++ {
		for j := 0; j < len(aAkhir); j++ {
			if aAWal[i] == aAkhir[j] {
				hasil = "merupakan palindrome"
			} else {
				hasil = "bukan palindrome"
			}
		}
	}
	//cetak hasil
	fmt.Println(hasil)
	//cara trainer
	for i := len(aAWal) - 1; i >= 0; i-- {
		sAkhir += aAWal[i]
	}

	if sNama == sAkhir {
		fmt.Println("Merupakan palindrome")
	} else {
		fmt.Println("Bukan palindrome")
	}

}
