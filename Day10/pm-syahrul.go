package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {

	//the quick brown fox jumps over the lazy dog
	//the quick brown fox jumps over the sleep dog
	//program pangram
	//deklarasi variabel
	var asci byte = 'a'
	var aAbjad []string
	var hasil string
	var conter int
	baca := bufio.NewReader(os.Stdin)
	var sKalimat string
	var fBen bool
	//input kalimat pangram
	fmt.Print("nama :")
	in1, _ := baca.ReadString('\n')
	sKalimat = in1
	//hilangkan enter
	sKalimat = strings.TrimSuffix(sKalimat, "\r\n")
	//hilangkan spasi
	sKalimat = strings.ReplaceAll(sKalimat, " ", "")
	//buat semua kalimat menjadi kecil
	sKalimat = strings.ToLower(sKalimat)
	//cacah kalimat dan ubah menjadi array
	aAWal := strings.Split(sKalimat, "")
	//buat array abjad dari a-z
	for i := 1; i <= 26; i++ {
		aAbjad = append(aAbjad, string(asci))
		asci++
	}
	//proses pengecekan pangram
	for i := 0; i < len(aAbjad); i++ { //looping data abjad

		for j := 0; j < len(aAWal); j++ { //looping kalimat
			//cek kesamaan antara abjad dengan kalimat
			if aAbjad[i] == aAWal[j] {
				conter++ //jika abjad bernilai sama maka conter di tambakan 1
				break
			}

		}
		if len(aAbjad) == conter {
			fBen = true // set nilai fBen menjadi true
		}
	}
	//cetak hasil pangram
	//jika bernilai true maka
	if fBen == true {
		hasil = "merupakan pangram"
	} else { //jika false maka
		hasil = "bukan pangram"
	}
	//hasil untuk func dari google
	var beda string
	if pangram(sKalimat) == true {
		beda = "merupakan pangram"
	} else { //jika false maka
		beda = "bukan pangram"
	}

	fmt.Println(hasil)
	fmt.Println(beda)

}

// dari google
// masih harus di mengerti dahulu cara dapet nilainya gimana
// tidak tau arti dari -1 itu apa dalam perubahan nilai dari string ke rune
func pangram(str string) bool {
	const Alphabets string = "abcdefghijklmnopqrstuvqxyz"
	str = strings.ToLower(strings.TrimSpace(str))
	if str == "" {
		return false
	}

	for _, v := range Alphabets {
		if strings.IndexRune(str, v) < 0 {
			return false
		}
	}
	return true
}
