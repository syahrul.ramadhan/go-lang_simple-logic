package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	//program mencacah string menjadi array
	var sNama string
	baca := bufio.NewReader(os.Stdin)
	//inputkan kalimat
	fmt.Print("inputkan nama panjang :")
	in1, _ := baca.ReadString('\n')
	sNama = in1
	//cacah kalimat di setiap spasi menjadi array
	arr := strings.Split(sNama, " ")
	//cetak kalimat yang telah menjadi array
	for i := 0; i < len(arr); i++ {
		fmt.Println(arr[i], " ")
	}
}
