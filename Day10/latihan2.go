package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {

	//program membalikan kalimat
	//deklarasi variabel
	var sNama string
	baca := bufio.NewReader(os.Stdin)
	//input kalimat
	fmt.Print("inputkan nama panjang :")
	in1, _ := baca.ReadString('\n')
	sNama = in1
	//hilangkan \n
	sNama = strings.TrimSuffix(sNama, "\r\n")
	//cacah kalimat menjadi array
	arr := strings.Split(sNama, "")
	//proses membalikan kalimat
	for i := len(arr) - 1; i >= 0; i-- {
		fmt.Print(arr[i])
	}
}
