package main

import (
	"fmt"
	"strings"
)

func main() {
	//string operation
	//Substring by slice
	nama := "Golang ya"

	getFirstName := nama[0:1]
	getLastName := nama[len(nama)-1:]
	fmt.Println(nama)
	fmt.Println(getFirstName)
	fmt.Println(getLastName)

	//replace all
	removeSpaceAll := strings.ReplaceAll(nama, " ", "?")
	removeSpace := strings.Replace(nama, "a", "i", 2)
	fmt.Println(removeSpaceAll)
	fmt.Println(removeSpace)

	//split
	arraySplit := strings.Split(nama, " ")
	fmt.Println(arraySplit)
}
