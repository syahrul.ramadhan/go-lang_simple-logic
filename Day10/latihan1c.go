package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	//program string operasi
	//menampilkan bintang sesuai dengan jumlah kata yang dimasukkan
	var sNama string
	baca := bufio.NewReader(os.Stdin)
	var bintang string
	var tampung string
	//input string
	fmt.Print("inputkan nama panjang :")
	in1, _ := baca.ReadString('\n')
	sNama = in1
	//melakukan trim pada variabel nama untuk menghilangkan \n
	sNama = strings.TrimSuffix(sNama, "\r\n")
	//melakukan split data menjadi slice atau array
	arr := strings.Split(sNama, " ")

	//proses melakukan perulangan dan pengambilan data
	for i := 0; i < len(arr); i++ {
		bintang = ""
		for k := 0; k < len(arr[i]); k++ {
			bintang += "*"
		}
		//cetak data sesuai jumlah string
		tampung = strings.ToUpper(arr[i])
		fmt.Println(tampung[0:1], bintang, tampung[len(tampung)-1:])
	}
}
