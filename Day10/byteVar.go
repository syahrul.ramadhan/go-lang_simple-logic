package main

import (
	"fmt"
)

func main() {
	//program byte menjadi sebuah huruf
	//deklarasi variabel
	var cKarakter byte = 'A'
	var aAbjad []string
	var sNama string
	var tNama string
	for i := 0; i < 26; i++ {
		aAbjad = append(aAbjad, string(cKarakter))
		cKarakter++
	}
	fmt.Print("byte : ", cKarakter, "\n")
	fmt.Print("byte translate : ", string(cKarakter), "\n")
	fmt.Println(aAbjad)

	for i := 0; i < len(aAbjad); i++ {
		fmt.Print("Masukkan karakter :")
		fmt.Scanln(&sNama)
		if sNama == aAbjad[i] {
			tNama = aAbjad[i]
		}
		fmt.Print(tNama)
	}
}
