package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	baca := bufio.NewReader(os.Stdin)
	var nama string
	var aVocal []string
	var aKonsonan []string
	var tampung string
	var tampung2 string
	fmt.Print("inputkan n :")
	in1, _ := baca.ReadString('\n')
	nama = in1
	nama = strings.TrimSuffix(nama, "\r\n")
	nama = strings.ReplaceAll(nama, " ", "")
	arr := strings.Split(nama, "")

	for i := 0; i < len(arr); i++ {
		if arr[i] == "a" || arr[i] == "i" || arr[i] == "u" || arr[i] == "e" || arr[i] == "o" {
			aVocal = append(aVocal, arr[i])
		} else {
			aKonsonan = append(aKonsonan, arr[i])
		}
	}

	for i := len(aVocal); i > 0; i-- {
		for j := 1; j < i; j++ {
			if aVocal[j-1] > aVocal[j] {
				tampung = aVocal[j]
				aVocal[j] = aVocal[j-1]
				aVocal[j-1] = tampung
			}
		}
	}

	for i := len(aKonsonan); i > 0; i-- {
		for j := 1; j < i; j++ {
			if aKonsonan[j-1] > aKonsonan[j] {
				tampung2 = aKonsonan[j]
				aKonsonan[j] = aKonsonan[j-1]
				aKonsonan[j-1] = tampung2
			}
		}
	}

	fmt.Print("huruf vocal :")
	for i := 0; i < len(aVocal); i++ {
		fmt.Print(aVocal[i], " ")
	}
	fmt.Println()
	fmt.Print("huruf konsonan : ")
	for i := 0; i < len(aKonsonan); i++ {
		fmt.Print(aKonsonan[i], " ")
	}
}
