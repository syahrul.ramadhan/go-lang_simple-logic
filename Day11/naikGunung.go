package main

import (
	f "fmt"
	"strings"
)

func main() {
	var jalan string = "NNTNNNTTTTTNTTTNTN"
	var aJalan = strings.Split(jalan, "")
	var mdpl int = 0
	var mdplSebelum int = 0
	var gunung, lembah int = 0, 0
	//jejak per aJalan
	for i := 0; i < len(aJalan); i++ {
		//cek naik apa turun
		if aJalan[i] == "N" {
			mdplSebelum = mdpl
			mdpl++
		}
		if aJalan[i] == "T" {
			mdplSebelum = mdpl
			mdpl--
		}
		//cata gungung dan lembah
		if mdpl == 0 && i > 0 && mdplSebelum > 0 {
			gunung++
		}
		if mdpl == 0 && i > 0 && mdplSebelum < 0 {
			lembah++
		}

	}
	f.Print("Gunung : ", gunung, "\n")
	f.Print("Lembah : ", lembah, "\n")
}
