package main

import (
	f "fmt"
	m "math/rand"
	t "time"
)

func main() {
	var min, max int = 0, 9
	var acak, acak2, kotak, kartu, taruhan int
	var kartuCPU int
	var stop bool = true
	var ulang string = ""
	// var pilihPlayer string = ""
	// var pilihCPU string = ""

	f.Print("Jumlah kartu atau gambaran : ")
	f.Scanln(&kartu)

	kartuCPU = kartu

	for stop {
		f.Print("Jumlah tawaran : ")
		f.Scanln(&taruhan)
		m.Seed(t.Now().UnixNano())
		acak = m.Intn(max-min) + min
		acak2 = m.Intn(max-min) + min
		f.Println("==========")
		f.Println()
		f.Println("Angka telah di acak")
		f.Print("Pilih kotak A / B (*pilih 1 untuk kotak A dan pilih 2 untuk kotak B) ?  ")
		f.Scanln(&kotak)
		f.Println()

		f.Println("==========")
		f.Println()

		switch kotak {
		case 1:
			f.Println("Angka kotak A : ", acak)
			f.Println("Angka Kotak B : ", acak2)
			if acak > acak2 {
				f.Println("You Win!")
				kartu = kartu + taruhan
				kartuCPU = kartuCPU - taruhan
			} else {
				f.Println("You Lose!")
				kartu = kartu - taruhan
				kartuCPU = kartuCPU + taruhan

			}
		case 2:
			f.Println("Angka Kotak B : ", acak)
			f.Println("Angka Kotak B : ", acak2)
			if acak2 > acak {
				f.Println("You Win!")
				kartu = kartu + taruhan
				kartuCPU = kartuCPU - taruhan
			} else {
				f.Println("You Lose!")
				kartu = kartu - taruhan
				kartuCPU = kartuCPU + taruhan
			}
		}
		f.Println("Jumlah Kartu Player:", kartu)
		f.Println("Jumlah Kartu CPU:", kartuCPU)
		f.Println()

		if kartu > 0 && kartuCPU > 0 {
			f.Print("Menyerah (y/n)? ")
			f.Scanln(&ulang)
			switch ulang {
			case "y":
				stop = false
			case "n":
				stop = true
			}
		} else if kartu <= 0 {
			f.Println("Kartu Player habis!!")
			f.Println("CPU WIN!")
			stop = false
		} else if kartuCPU <= 0 {
			f.Println("Kartu CPUs habis!!")
			f.Println("Player WIN!")
			stop = false
		}

	}

	f.Println()
	f.Println("Permainan Selesai")
	f.Println("==========")
	f.Println("END")
}
