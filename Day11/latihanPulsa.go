package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	var text string = "Beli Pulsa Rp. 75.000"
	var hitPoint int
	var arrPoint = []int{}
	fmt.Println(text)

	// memcah string menjadi nominal
	text = text[strings.IndexByte(text, '.'):] //slice string
	text = strings.ReplaceAll(text, " ", "")   //hilangkan semua spasi
	text = strings.ReplaceAll(text, ".", "")   //hilangkan semua titik
	nil1, _ := strconv.Atoi(text)              //convert string menjadi integer

	// proses
	for i := nil1; i > 10000; i-- {
		if nil1 != 0 {
			if nil1 >= 30001 && nil1 <= 75000 {
				nil1 = nil1 - 45000
				hitPoint = (45000 / 1000) * 2
				arrPoint = append(arrPoint, hitPoint)
			} else if nil1 >= 10001 && nil1 <= 20000 {
				nil1 = nil1 - 10000
				hitPoint = 10000 / 1000
				arrPoint = append(arrPoint, hitPoint)
			} else if nil1 >= 10001 && nil1 <= 30000 {
				nil1 = nil1 - 20000
				hitPoint = 20000 / 1000
				arrPoint = append(arrPoint, hitPoint)
			} else if nil1 <= 10000 {
				nil1 = nil1 - 10000
				hitPoint = 0
				arrPoint = append(arrPoint, hitPoint)
				break
			}
		} else {
			hitPoint = 0
			arrPoint = append(arrPoint, hitPoint)
			break
		}
	}
	// cetak hasil
	var point int
	for i := len(arrPoint) - 1; i >= 0; i-- {
		if i == 0 {
			fmt.Print(arrPoint[i], " = ")
		} else {
			fmt.Print(arrPoint[i], " + ")
		}
		point += arrPoint[i]
	}
	fmt.Print(point)

}
