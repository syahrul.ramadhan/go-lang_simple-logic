package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	baca := bufio.NewReader(os.Stdin)
	var nama string
	var bintang string
	var sTampung string
	fmt.Print("inputkan nama panjang :")
	in1, _ := baca.ReadString('\n')
	nama = in1
	nama = strings.TrimSuffix(nama, "\r\n")
	arr := strings.Split(nama, " ")

	for i := 0; i < len(arr); i++ {
		bintang = ""
		for j := 0; j < 3; j++ {
			bintang += "*"
		}
		sTampung = arr[i]
		fmt.Print(sTampung[0:1], bintang, sTampung[len(sTampung)-1:], " ")
	}
}
