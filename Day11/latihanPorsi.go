package main

import (
	"fmt"
	f "fmt"
	"strconv"
	s "strings"
)

func main() {
	var ygMakan string = "Laki-laki dewasa = 4 orang; Perempuan dewasa = 1 orang; Balita = 1 orang; Laki-laki dewasa = 1 orang"
	ygMakan = s.ReplaceAll(ygMakan, "orang", "") //hapus string orang
	var aYgMakan = s.Split(ygMakan, ";")         //pecah berdasar karakter ;
	for i := 0; i < len(aYgMakan); i++ {         //hapus spasi awal akhir
		aYgMakan[i] = s.TrimSpace(aYgMakan[i])
	}
	var aYgMakanUniq = []string{}
	var aYgMakanNonDuplik = []string{}

	//ambil tanpa nilai
	for i := 0; i < len(aYgMakan); i++ {
		a := aYgMakan[i][0 : len(aYgMakan[i])-4]
		aYgMakanUniq = append(aYgMakanUniq, a)
	}

	//sort
	for i := 0; i < len(aYgMakanUniq)-1; i++ {
		if aYgMakanUniq[i] > aYgMakanUniq[i+1] {
			w := aYgMakanUniq[i]
			aYgMakanUniq[i] = aYgMakanUniq[i+1]
			aYgMakanUniq[i+1] = w
		}
	}

	//remove duplikat
	for i := 0; i < len(aYgMakanUniq); i++ {
		fAda := false
		for x := 0; x < len(aYgMakanNonDuplik); x++ {
			if aYgMakanUniq[i] == aYgMakanNonDuplik[x] {
				fAda = true
				break
			}
		}
		if fAda == false {
			aYgMakanNonDuplik = append(aYgMakanNonDuplik, aYgMakanUniq[i])
		}
	}
	jOrang := 0
	for i := 0; i < len(aYgMakanNonDuplik); i++ {
		q := 0
		qty := 0
		fmt.Print(aYgMakanNonDuplik[i], " : ")
		for x := 0; x < len(aYgMakan); x++ {
			if aYgMakanNonDuplik[i] == aYgMakan[x][0:s.IndexByte(aYgMakan[x], '=')-1] {
				q, _ = strconv.Atoi(aYgMakan[x][s.IndexByte(aYgMakan[x], '=')+2:]) // ambil nilai setelah karakter =
				qty = qty + q
			}
		}
		fmt.Println(qty)
		jOrang = jOrang + qty
	}
	fmt.Print("jumlah orang : ", jOrang)
	fmt.Println()
	fmt.Println()
	var pLaki, pPerempuan, pRemaja, pAnak, pBalita float32 = 0.0, 0.0, 0.0, 0.0, 0.0
	var jPorsi float32 = 0
	for i := 0; i < len(aYgMakanNonDuplik); i++ {
		q := 0
		qty := 0
		for x := 0; x < len(aYgMakan); x++ {
			if aYgMakanNonDuplik[i] == aYgMakan[x][0:s.IndexByte(aYgMakan[x], '=')-1] {
				q, _ = strconv.Atoi(aYgMakan[x][s.IndexByte(aYgMakan[x], '=')+2:]) // ambil nilai setelah karakter =
				qty = qty + q
			}
		}
		if aYgMakanNonDuplik[i] == "Laki-laki dewasa" {
			pLaki = float32(qty) * 2
		}
		if aYgMakanNonDuplik[i] == "Perempuan dewasa" {
			if jOrang%2 != 0 && jOrang > 5 {
				pPerempuan = float32(qty) * 2
			} else {
				pPerempuan = float32(qty) * 1
			}
		}
		if aYgMakanNonDuplik[i] == "Remaja" {
			pRemaja = float32(qty) * 1
		}
		if aYgMakanNonDuplik[i] == "Anak-anak" {
			pAnak = float32(qty) * 0.5
		}
		if aYgMakanNonDuplik[i] == "Balita" {
			pBalita = float32(qty) * 1
		}
		jPorsi = pLaki + pPerempuan + pRemaja + pAnak + pBalita
	}
	f.Print("Total porsi : ", jPorsi)

}
