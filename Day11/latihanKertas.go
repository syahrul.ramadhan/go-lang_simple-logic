package main

import (
	"fmt"
)

func main() {
	var kertas int
	var hasil int

	fmt.Print("A ?")
	fmt.Scanln(&kertas)

	switch kertas {
	case 5:
		hasil = 2
	case 4:
		hasil = 4
	case 3:
		hasil = 8
	case 2:
		hasil = 16
	case 1:
		hasil = 32
	case 0:
		hasil = 48
	}

	fmt.Print("lembar kertas A6 :", hasil, " untuk menjadi A", kertas)

}
