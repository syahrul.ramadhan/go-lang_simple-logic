package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	//the quick brown fox jumps over the lazy dog
	//the quick brown fox jumps over the sleep dog
	var Kalimat string
	var kar byte = 'a'
	var alpa []string
	var ben bool = true
	baca := bufio.NewReader(os.Stdin)
	//input kalimat
	fmt.Print("kalimat :")
	in1, _ := baca.ReadString('\n')
	Kalimat = in1
	//hilangkan \n
	Kalimat = strings.TrimSuffix(Kalimat, "\r\n")
	Kalimat = strings.ToLower(Kalimat)
	for i := 0; i < 26; i++ {
		alpa = append(alpa, string(kar))
		kar++
	}
	for i := 0; i < len(alpa); i++ {
		if !strings.Contains(Kalimat, alpa[i]) {
			ben = false
			break
		}
	}

	if ben == false {
		fmt.Println("Bukan pangram")
	} else {
		fmt.Println("merupakan pangram")
	}

}
