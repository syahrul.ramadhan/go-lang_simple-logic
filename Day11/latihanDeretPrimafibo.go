package main

import f "fmt"

func main() {
	var pDeret int = 10
	var aPrima = []int{}
	var aFibo = []int{}

	var i int = 0
	var x int = 0
	var jBagi int = 0
	var jPrima int = 0
	var jumlah int = 0
	//loop1,buat deret tak hingga
	for {
		//loop2, cek jum bagi
		jBagi = 0
		for x = 1; x <= i; x++ {
			if i%x == 0 {
				jBagi++
			}
		}
		if jBagi == 2 {
			aPrima = append(aPrima, i)
			jPrima++
		}

		//control loop1
		if jPrima == pDeret {
			break
		}
		i++
	}
	//proses fibo
	var fibo int = 0
	var a int = 0
	var b int = 1
	for i := 0; i < pDeret; i++ {
		a = b
		b = fibo
		fibo = a + b
		aFibo = append(aFibo, fibo)
	}

	f.Print(aPrima, "\n")
	f.Print(aFibo, "\n")
	//cetak index ganjil
	for g := 0; g < len(aPrima); g++ {
		if g%2 != 0 {
			f.Print(aPrima[g], " + ", aFibo[g], " ")
		}
	}
	f.Println()
	//untuk jumlah
	for g := 0; g < len(aPrima); g++ {
		if g%2 != 0 {
			jumlah = aPrima[g] + aFibo[g]
			f.Print(jumlah, ",")
		}

	}
}
