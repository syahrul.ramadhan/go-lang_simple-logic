package main

import "fmt"

func main() {
	var fibo, a, b = 0, 1, 1
	for i := 0; i < 10; i++ {
		if a%2 == 0 {
			fmt.Print(a, " ")
		}
		fibo = a + b
		a = b
		b = fibo
	}
}
