package main

import (
	"fmt"
	"time"
)

func main() {
	//"28/01/2020 07:00:00"
	var layoutFormat, tanggalPinjam, tanggalKembali string
	var dateA time.Time // tipe data time
	var dateB time.Time

	layoutFormat = "2006-01-02 15:04:05" // ini untuk format waktu, isi bebas cuma penanda harus jelas
	tanggalPinjam = "2015-09-01 01:04:00"
	tanggalKembali = "2015-09-02 01:04:00"
	dateA, _ = time.Parse(layoutFormat, tanggalPinjam)
	dateB, _ = time.Parse(layoutFormat, tanggalKembali)
	difference := dateB.Sub(dateA)

	fmt.Print("Jam keluar:", difference.Hours())
}
