package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

func main() {
	var input1, input2 string
	var times1, times2 time.Time
	var masuk, keluar string
	var hasil int
	var flag bool
	var conv int
	var jam float64
	formatLayout := "2006 01 02 15:04:05"
	baca := bufio.NewReader(os.Stdin)
	fmt.Print("inputkan waktu masuk :")
	in1, _ := baca.ReadString('\n')
	fmt.Print("inputkan waktu keluar :")
	in2, _ := baca.ReadString('\n')
	input1 = in1
	input2 = in2
	input1 = strings.TrimSuffix(input1, "\r\n")
	input2 = strings.TrimSuffix(input2, "\r\n")
	input1 = strings.ToLower(input1)
	input2 = strings.ToLower(input2)
	arr1 := strings.Split(input1, " ")
	arr2 := strings.Split(input2, " ")
	masuk = konversi(arr1)
	keluar = konversi(arr2)
	times1, _ = time.Parse(formatLayout, masuk)
	times2, _ = time.Parse(formatLayout, keluar)
	banding := times2.Sub(times1)
	jam = banding.Hours()
	conv = int(jam)
	for flag == false {
		if conv <= 8 {
			hasil += 1000
			conv = conv - 8
		} else if conv > 8 || conv <= 24 {
			hasil += 8000
			conv = conv - 8

		} else if jam > 24 {
			hasil += 15000
			conv -= 24
		}
		if conv == 0 {
			flag = true
		}
	}

	fmt.Println("jumlah bayar :", hasil)
}

func konversi(s []string) string {
	arr1 := s
	var konversi string
	if arr1[1] == "januari" {
		arr1[1] = "01"
	} else if arr1[1] == "februari" {
		arr1[1] = "02"
	} else if arr1[1] == "maret" {
		arr1[1] = "03"
	} else if arr1[1] == "april" {
		arr1[1] = "04"
	} else if arr1[1] == "mei" {
		arr1[1] = "05"
	} else if arr1[1] == "juni" {
		arr1[1] = "06"
	} else if arr1[1] == "juli" {
		arr1[1] = "07"
	} else if arr1[1] == "agustus" {
		arr1[1] = "08"
	} else if arr1[1] == "september" {
		arr1[1] = "09"
	} else if arr1[1] == "oktober" {
		arr1[1] = "10"
	} else if arr1[1] == "november" {
		arr1[1] = "11"
	} else if arr1[1] == "desember" {
		arr1[1] = "12"
	}
	konversi = strings.Join(arr1, " ")
	return konversi
}
