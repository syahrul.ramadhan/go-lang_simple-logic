package main

import (
	"fmt"
	"strconv"
	s "strings"
)

func main() {
	var ygBuah string = "Apel:3, Pisang:1, Apel:3, Apel:5, Jeruk:8,Mangga:1"

	var aYgbuah = s.Split(ygBuah, ",")  //pecah berdasar karakter ;
	for i := 0; i < len(aYgbuah); i++ { //hapus spasi awal akhir
		aYgbuah[i] = s.TrimSpace(aYgbuah[i])
	}
	var aYgBuahUniq = []string{}
	var aYgBuahNonDuplik = []string{}

	//ambil tanpa nilai
	for i := 0; i < len(aYgbuah); i++ {
		a := aYgbuah[i][0 : len(aYgbuah[i])-2]
		aYgBuahUniq = append(aYgBuahUniq, a)
	}

	//sort
	for i := 0; i < len(aYgBuahUniq)-1; i++ {
		if aYgBuahUniq[i] > aYgBuahUniq[i+1] {
			w := aYgBuahUniq[i]
			aYgBuahUniq[i] = aYgBuahUniq[i+1]
			aYgBuahUniq[i+1] = w
		}
	}

	//remove duplikat
	for i := 0; i < len(aYgBuahUniq); i++ {
		fAda := false
		for x := 0; x < len(aYgBuahNonDuplik); x++ {
			if aYgBuahUniq[i] == aYgBuahNonDuplik[x] {
				fAda = true
				break
			}
		}
		if fAda == false {
			aYgBuahNonDuplik = append(aYgBuahNonDuplik, aYgBuahUniq[i])
		}
	}

	for i := 0; i < len(aYgBuahNonDuplik); i++ {
		q := 0
		qty := 0

		fmt.Print(aYgBuahNonDuplik[i], " : ")
		for x := 0; x < len(aYgbuah); x++ {
			if aYgBuahNonDuplik[i] == aYgbuah[x][0:s.IndexByte(aYgbuah[x], ':')] {
				q, _ = strconv.Atoi(aYgbuah[x][s.IndexByte(aYgbuah[x], ':')+1:]) // ambil nilai setelah karakter =
				qty = qty + q
			}
		}
		fmt.Println(qty)
	}
}
