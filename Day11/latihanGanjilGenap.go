package main

import "fmt"

func main() {

	var deret int
	var iGanjil int
	var iGenap int
	var ganjil = 1
	var genap = 2

	fmt.Print("input n : ")
	fmt.Scanln(&deret)

	if deret%2 != 0 {
		iGanjil = (deret / 2) + 1
		iGenap = deret / 2
	} else {
		iGanjil = deret / 2
		iGenap = deret / 2
	}

	for i := 0; i < iGanjil; i++ {
		fmt.Print(ganjil, " ")
		ganjil += 2
	}
	fmt.Println()
	for i := 0; i < iGenap; i++ {
		fmt.Print(genap, " ")
		genap += 2
	}

}
