package main

import "fmt"

func main() {

	var konversi float32
	var hasil float32
	var pilih int

	fmt.Println("jenis konversi :")
	fmt.Println("1. botol ke cangkir")
	fmt.Println("2. botol ke gelas")
	fmt.Println("3. botol ke teko")
	fmt.Println("4. gelas ke botol")
	fmt.Println("5. gelas ke cangkir")
	fmt.Println("6. gelas ke teko")
	fmt.Println("7. teko ke cangkir")
	fmt.Println("8. teko ke gelas")
	fmt.Println("9. teko ke botol")
	fmt.Println("10. cangkir ke botol")
	fmt.Println("11. cangkir ke gelas")
	fmt.Println("12. cangkir ke teko")
	fmt.Print("pilih konversi :")
	fmt.Scanln(&pilih)
	fmt.Print("masukkan jumlah konversi:")
	fmt.Scanln(&konversi)

	switch pilih {
	case 1:
		hasil = konversi * 2 * 2.5
	case 2:
		hasil = konversi * 2
	case 3:
		hasil = (konversi * 2 * 2.5) / 25
	case 4:
		hasil = konversi / 2
	case 5:
		hasil = konversi * 2.5
	case 6:
		hasil = (konversi * 2.5) / 25
	case 7:
		hasil = konversi * 25
	case 8:
		hasil = (konversi * 25) / 2.5
	case 9:
		hasil = konversi * 25 / 2.5 / 2
	case 10:
		hasil = konversi / 2.5 / 2
	case 11:
		hasil = konversi / 2.5
	case 12:
		hasil = konversi / 25
	}

	fmt.Println("hasil : ", hasil)

}
