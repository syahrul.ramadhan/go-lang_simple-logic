package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	//program geser kalimat
	//deklarasi variabel
	var sKalimat string
	var tampung string
	baca := bufio.NewReader(os.Stdin)
	//input kalimat
	fmt.Print("kalimat :")
	in1, _ := baca.ReadString('\n')
	sKalimat = in1

	//hilangkan \n
	sKalimat = strings.TrimSuffix(sKalimat, "\r\n")
	//string di ubah menjadi array
	aAWal := strings.Split(sKalimat, "")
	//proses geser kalimat
	for i := 0; i < len(aAWal)-1; i++ {
		tampung = aAWal[i]
		aAWal[i] = aAWal[i+1]
		aAWal[i+1] = tampung
	}
	//cetak hasil geser kalimat
	for i := 0; i < len(aAWal); i++ {
		fmt.Print(aAWal[i])
	}
}
