package main

import "fmt"

func main() {
	var panjang int
	var aGanjil []int
	var aGenap []int

	var ganjil int = 3

	var genap int = -2
	fmt.Print("Panjang Array :")
	fmt.Scanln(&panjang)

	for i := 0; i < panjang; i++ {
		aGanjil = append(aGanjil, ganjil)
		ganjil += 2
		aGenap = append(aGenap, genap)
		genap += -2
	}

	fmt.Println()
	fmt.Print("hasil :")
	for i := 0; i < len(aGanjil); i++ {
		fmt.Print(aGanjil[i]+aGenap[i], " ")
	}

}
