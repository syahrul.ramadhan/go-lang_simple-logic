package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

func main() {
	//var format12 = "03:04:00 PM"
	// var format24 = "15:00:04"
	// conv12, _ := time.Parse(format24, "04:50:04PM")
	// fmt.Println(conv12.Format(format24))

	// //conv24, _ := time.Parse(format24, "15:00:04")
	// //fmt.Println(conv24.Format(format12))

	layout1 := "03:04 PM"
	layout2 := "15:04"
	// t, err := time.Parse(layout1, "08:05PM")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return
	// }
	// fmt.Println(t.Format(layout1))
	// fmt.Println(t.Format(layout2))

	baca := bufio.NewReader(os.Stdin)
	fmt.Print("inputkan waktu masuk :")
	in1, _ := baca.ReadString('\n')
	input := in1
	input = strings.TrimSuffix(input, "\r\n")

	hasil, _ := time.Parse(layout1, input)

	fmt.Println("output :", hasil.Format(layout2))
}
