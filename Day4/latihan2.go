package main

import (
	"fmt"
	"strings"
)

func main() {
	var huruf string
	var data string
	fmt.Print("inputkan huruf :")
	fmt.Scanln(&huruf)
	huruf = strings.ToLower(huruf)

	// if huruf == "a" || huruf == "i" || huruf == "e" || huruf == "o" || huruf == "u" {
	// 	data = "vokal"
	// } else {
	// 	data = "konsonan"
	// }

	switch {
	case huruf == "a" || huruf == "i" || huruf == "u" || huruf == "e" || huruf == "o":
		data = "vokal"
	default:
		data = "konsonan"
	}

	fmt.Println("huruf", huruf, "merupakan", data)
}
