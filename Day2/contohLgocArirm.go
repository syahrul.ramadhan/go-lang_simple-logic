package main

import "fmt"

func main() {
	// ==
	fmt.Println("apakah 5 sama dengan 6", 5 == 6)
	fmt.Println("apakah 5 sama dengan 6", 3 == 3)
	fmt.Println()
	//!=
	fmt.Println("apakah 5 tidak sama dengan 6", 5 != 6)
	fmt.Println("apakah 5 tidak sama dengan 6", 3 != 3)
	fmt.Println()
	//<
	fmt.Println("apakah 5 kurang dari 6", 5 < 6)
	fmt.Println("apakah 5 kurang dari sama dengan 6", 5 <= 6)
	fmt.Println()
}
