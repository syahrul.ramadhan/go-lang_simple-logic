package main

import "fmt"

func main() {
	p, l, t := 0, 0, 0

	fmt.Print("masukkan panjang :")
	fmt.Scanln(&p)
	fmt.Print("masukkan lebar :")
	fmt.Scanln(&l)
	fmt.Print("masukkan tinggi :")
	fmt.Scanln(&t)
	volume(p, l, t, hitung(p, l, t))
}

func hitung(p int, l int, t int) int {
	var total int
	total = p * l * t
	return total
}

func volume(p int, l int, t int, hasil int) {
	fmt.Println("Volume", p, "*", l, "*", t, "=", hasil)
}
