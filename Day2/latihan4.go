package main

import "fmt"

func main() {
	//deklarasi
	var val1, val2, val3, total, kurang, sisa, persen float32
	//input
	fmt.Print("Banyak uang sepuluh ribu :")
	fmt.Scanln(&val1)
	fmt.Print("Banyak uang lima ribu :")
	fmt.Scanln(&val2)
	fmt.Print("Banyak uang dua puluh ribu :")
	fmt.Scanln(&val3)
	fmt.Print("Uang belanja :")
	fmt.Scanln(&kurang)
	//proses hitung
	total = (10_000 * val1) + (5_000 * val2) + (20_000 * val3)
	sisa = total - kurang
	persen = (sisa / total) * 100
	//output
	fmt.Println("Total uang : ", total)
	fmt.Println("Sisa Uang : ", sisa, " Tinggal :", persen, "%")

}
