package main

import (
	"fmt"
	"strconv"
)

func main() {
	nilai := "100"
	nilai2 := 0
	var nilai4 int = 4
	var nilai5 float32
	var nilai3 string
	nilai2, _ = strconv.Atoi(nilai) //conversi dari string ke int membutuhkan 2 penampung
	nilai3 = strconv.Itoa(nilai2)   // conversi dari int ke string membutuhkan 1 penampung
	nilai5 = float32(nilai4)
	fmt.Println("conversi dari string ke int", nilai2)
	fmt.Println("conversi dari int ke string", nilai3)
	fmt.Println("ini string ", nilai)
	fmt.Println("ini float32 ", nilai5)
}
