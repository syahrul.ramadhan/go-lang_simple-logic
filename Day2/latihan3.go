package main

import "fmt"

func main() {

	var persen, hJual, hBeli, total float32

	fmt.Print("Harga Beli :")
	fmt.Scanln(&hBeli)
	fmt.Print("Harga Jual :")
	fmt.Scanln(&hJual)
	total = hJual - hBeli
	persen = (total / hBeli) * 100
	fmt.Println("Jumlah Keuntungan :", total, "Rupiah", persen, "Persen")

}
