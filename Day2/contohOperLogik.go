package main

import (
	"fmt"
)

func main() {

	//dan
	kanan, kiri := true, true
	fmt.Println("hasil", kiri && kanan)
	kanan, kiri = true, false
	fmt.Println("hasil", kiri && kanan)
	kanan, kiri = false, false
	fmt.Println("hasil", kiri && kanan)
	fmt.Println("")

	//atau
	kanan, kiri = true, true
	fmt.Println("hasil", kiri || kanan)
	kanan, kiri = true, false
	fmt.Println("hasil", kiri || kanan)
	kanan, kiri = false, true
	fmt.Println("hasil", kiri || kanan)
	kanan, kiri = false, false
	fmt.Println("hasil", kiri || kanan)
}
