package main

import "fmt"

func main() {
	total, p, l, t := hitung()
	volume(p, l, t, total)
}

func hitung() (int, int, int, int) {
	total, p, l, t := 0, 0, 0, 0

	fmt.Print("masukkan panjang :")
	fmt.Scanln(&p)
	fmt.Print("masukkan lebar :")
	fmt.Scanln(&l)
	fmt.Print("masukkan tinggi :")
	fmt.Scanln(&t)
	total = p * l * t
	return total, p, l, t
}

func volume(p int, l int, t int, hasil int) {
	fmt.Println("Volume", p, "*", l, "*", t, "=", hasil)
}
