package main

import (
	"fmt"
)

func main() {
	bintang := "*"
	fmt.Println(bintang)
	bintang = bintang + "*"
	fmt.Println(bintang)
	bintang = bintang + "*"
	fmt.Println(bintang)
	bintang = bintang + "*"
	fmt.Println(bintang)
	bintang = bintang + "*"
	fmt.Println(bintang)
	bintang = bintang + "*"
	fmt.Println(bintang)
	bintang = bintang + "*"

	// perulangan()
	// dimensi()

}

// satu perulangan
// func perulangan() {
// 	bintang := ""
// 	for i := 1; i <= 6; i++ {
// 		bintang += "*"
// 		fmt.Println(bintang)
// 	}
// }

// dua perulangan
// func dimensi() {
// 	for i := 10; i >= 1; i-- {
// 		fmt.Println("")
// 		for j := 1; j <= 10; j++ {
// 			fmt.Print("*")
// 		}
// 	}
// }
