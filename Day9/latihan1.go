package main

import "fmt"

//program menghitung median dengan jumlah ganjil atau genap
func main() {
	//proses deklarasi
	var nilai = []int{9, 5, 1, 3, 6, 8, 7, 4, 2}
	var nilai2 = []int{9, 5, 1, 3, 6, 8, 7, 4, 2, 10, 11, 12}
	var wadah int
	var wadah2 int
	var me int
	var medianGenap1 int
	var medianGenap2 int
	var hasil float32
	//proses mengurutkan data
	for i := len(nilai); i > 0; i-- {
		for j := 1; j < i; j++ {
			if nilai[j-1] > nilai[j] {
				wadah = nilai[j]
				nilai[j] = nilai[j-1]
				nilai[j-1] = wadah
			}
		}
	}
	for i := len(nilai2); i > 0; i-- {
		for j := 1; j < i; j++ {
			if nilai2[j-1] > nilai2[j] {
				wadah2 = nilai2[j]
				nilai2[j] = nilai2[j-1]
				nilai2[j-1] = wadah2
			}
		}
	}
	//data telah terurutkan
	fmt.Print("Diurutkan :")
	for i := 0; i < len(nilai); i++ {
		fmt.Print(nilai[i], " ")
	}
	fmt.Println()
	fmt.Print("Diurutkan :")
	for i := 0; i < len(nilai2); i++ {
		fmt.Print(nilai2[i], " ")
	}
	//proses perhitungan median ganjil
	fmt.Println()
	me = len(nilai) / 2
	fmt.Println("median ganjil :", nilai[me])
	fmt.Println()
	//proses perhitungan median genap
	medianGenap1 = (len(nilai2) / 2) - 1
	medianGenap2 = (len(nilai2) + 1) / 2
	hasil = (float32(nilai2[medianGenap1]) + float32(nilai2[medianGenap2])) / 2
	fmt.Print("median Genap :", hasil)

}
