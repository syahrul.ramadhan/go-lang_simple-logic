package main

import "fmt"

func main() {

	//program menduplikat nilai array1 ke array2
	//deklarasi variabel
	var nilai1 = []int{2, 4, 6, 8}
	var nilai2 [4]int
	var tampung int
	//nilai array awal
	fmt.Print("array 1 :")
	for i := 0; i < len(nilai1); i++ {
		fmt.Print(nilai1[i])
	}
	fmt.Println()
	fmt.Print("array 2 :")
	for i := 0; i < len(nilai1); i++ {
		fmt.Print(nilai2[i])
	}
	//proses menduplikat nilai array
	for i := 0; i < len(nilai1); i++ {
		tampung = nilai2[i]
		nilai2[i] = nilai1[i]
		nilai1[i] = tampung
	}

	//nilai array setelah di proses
	fmt.Println()
	fmt.Println("copy")
	fmt.Print("array 1 :")
	for i := 0; i < len(nilai1); i++ {
		fmt.Print(nilai1[i])
	}
	fmt.Println()
	fmt.Print("array 2 :")
	for i := 0; i < len(nilai1); i++ {
		fmt.Print(nilai2[i])
	}
}
