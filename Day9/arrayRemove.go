package main

import "fmt"

func main() {
	var nilai = []int{1, 2, 1, 3, 4, 1, 5, 6, 2}
	var akhir = []int{}
	var fAda bool

	//cetak awal
	for i := 0; i < len(nilai); i++ {
		fmt.Print(nilai[i], " ")
	}
	fmt.Println()

	//looping baca element awal
	for i := 0; i < len(nilai); i++ {
		fAda = false
		//looping untuk cek di hasil
		for j := 0; j < len(akhir); j++ {
			//proses menampung hasil nilai flag supaya nilai
			//yang sama tidak dimasukkan kedalam wadah yang sama
			if nilai[i] == akhir[j] {
				fAda = true
				break
			}
		}
		//proses menampung data yang bernilai tidak sama
		if fAda == false {
			akhir = append(akhir, nilai[i])
		}
	}
	//cetak data yang telah terurutkan
	for i := 0; i < len(akhir); i++ {
		fmt.Print(akhir[i], " ")
	}
}
