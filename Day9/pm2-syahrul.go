package main

import "fmt"

func main() {
	//deklarasi variable
	var arr = []int{1, 2, 3, 4, 5, 6, 7}
	var wadah int
	//proses pemindahan nilai
	//atau geser nilai ke kiri
	for i := 0; i < len(arr)-1; i++ {
		wadah = arr[i]
		arr[i] = arr[i+1]
		arr[i+1] = wadah
	}
	//cetak data yang telah di proses
	for i := 0; i < len(arr); i++ {
		fmt.Print(arr[i], " ")
	}
}
