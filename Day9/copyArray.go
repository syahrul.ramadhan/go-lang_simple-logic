package main

import "fmt"

func main() {
	//deklarasi variabel
	var nilai1 = []int{2, 4, 6, 8}
	var nilai2 = []int{0, 0, 0, 0}
	//var tampung int
	fmt.Print("array 1 :")
	for i := 0; i < len(nilai1); i++ {
		fmt.Print(nilai1[i])
	}
	fmt.Println()
	fmt.Print("array 2 :")
	for i := 0; i < len(nilai1); i++ {
		fmt.Print(nilai2[i])
	}
	//proses perubahan data
	for i := 0; i < len(nilai1); i++ {
		nilai2[i] = nilai1[1]
		nilai1[i] = 0
	}
	//cetak data
	fmt.Println()
	fmt.Println("copy")
	fmt.Print("array 1 :")
	for i := 0; i < len(nilai1); i++ {
		fmt.Print(nilai1[i])
	}
	fmt.Println()
	fmt.Print("array 2 :")
	for i := 0; i < len(nilai1); i++ {
		fmt.Print(nilai2[i])
	}
}
