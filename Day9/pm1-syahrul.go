package main

import "fmt"

func main() {

	//program summary array atau menghitung nilai yang sama
	//dalam satu array
	var arr = []int{1, 2, 1, 3, 4, 1, 5, 6, 2}
	var arr2 []int
	var fAda bool
	var jumlah int
	for i := 0; i < len(arr); i++ {
		fAda = false
		//looping untuk cek di hasil
		for j := 0; j < len(arr2); j++ {
			if arr[i] == arr2[j] {
				fAda = true
				break
			}
		}
		if fAda == false {
			arr2 = append(arr2, arr[i])
		}

	}

	//proses cetak nilai
	for i := 0; i < len(arr2); i++ {
		fmt.Print("\n", arr2[i], " : ")
		for j := 0; j < len(arr); j++ {
			//penjumlahan jika ada nilai yang sama
			if arr2[i] == arr[j] {
				jumlah++
			}
		}
		//pengecekan jika data tersebut sudah bernilai atau
		//tidak bernilai 0
		if jumlah != 0 {
			fmt.Print(jumlah)
			jumlah = 0
		}
	}

}
