package main

import "fmt"

func main() {

	//program menghapus nilai yang sama dalam 1 array
	//deklarasi variabel
	var nilai1 = []int{1, 2, 1, 3, 4, 1, 5, 6, 2}
	var tampung int
	fmt.Print("Awal :")
	for i := 0; i < len(nilai1); i++ {
		fmt.Print(nilai1[i], " ")
	}
	//proses menghapus nilai yang sama
	for i := 0; i < len(nilai1); i++ {
		for j := 0; j < len(nilai1)-1; j++ {
			if nilai1[j] != nilai1[j+1] {
				if nilai1[j] > nilai1[j+1] {
					tampung = nilai1[j]
					nilai1[j] = nilai1[j+1]
					nilai1[j+1] = tampung
				}
			} else {
				//nilai1 = append(nilai1[:j], nilai1[j+1:]...)
				nilai1[j] = 0
			}
		}
	}
	//cetak array yang sudah tidak ada nilai kembar
	fmt.Println()
	fmt.Print("akhir :")
	for i := 0; i < len(nilai1); i++ {
		if nilai1[i] != 0 {
			fmt.Print(nilai1[i], " ")
		}
	}
}

//121341562
