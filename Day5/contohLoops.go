package main

import "fmt"

func main() {
	//ultimate loops
	for {
		fmt.Println("ult Loops")
		break //terminate loop, atau pemberhenti loop
	}
	//for
	var nilai int = 0 // variable global
	for i := 0; i < 5; i++ {
		nilai++
	}
	fmt.Println(nilai)
	//for dengan variable local
	for i := 0; i < 5; i++ {
		var s string = "nilai"
		fmt.Println(s, i)
	}

	var num = 0
	for num < 5 {
		fmt.Println(num)
		num++
	}

	for i := 0; i < 5; i++ {
		fmt.Print("")
		for j := 0; j < i; j++ {
			fmt.Println("*")
		}
	}
}
