package main

import "fmt"

func main() {
	var nilai int

	fmt.Print("panjang deret ?")
	fmt.Scanln(&nilai)

	//ganjil
	for i := 1; i <= nilai; i++ {
		if i%2 == 1 {
			fmt.Println(i, "ganjil")
		} else {
			fmt.Println(i)
		}
	}
	fmt.Println()
	//genap
	for j := 1; j <= nilai; j++ {
		if j%2 == 0 {
			fmt.Println(j, "genap")
		} else {
			fmt.Println(j)
		}
	}
}
