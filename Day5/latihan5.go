package main

import "fmt"

func main() {
	var nilai int
	var genap = 2
	var ganjil = 1
	fmt.Print("Deret ?")
	fmt.Scanln(&nilai)

	//genap
	for i := 0; i < nilai; i++ {
		fmt.Print(genap, " ")
		genap += 2
	}
	fmt.Println()
	//ganjil
	for j := 0; j < nilai; j++ {
		fmt.Print(ganjil, " ")
		ganjil += 2
	}
}
