package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	var nama string
	var nilai int
	baca := bufio.NewReader(os.Stdin)
	fmt.Print("nama ?")
	init1, _ := baca.ReadString('\n')

	nama = strings.TrimRight(init1, "\r\n")

	fmt.Print("Bilangan ?")
	fmt.Scanln(&nilai)

	for i := nilai; i > 0; i-- {
		fmt.Print(i, ".", nama, "\n")
	}
}
