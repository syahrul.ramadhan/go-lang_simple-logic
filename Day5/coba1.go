package main

import "fmt"

func main() {
	var nilai int
	var isi = 1

	fmt.Print("Deret ? ")
	fmt.Scanln(&nilai)

	//loop patern 1
	for i := 1; i <= nilai; i++ {
		if i%3 == 0 {
			fmt.Print("#", " ")
		} else {
			fmt.Print(isi, " ")
			isi++
		}
	}
	fmt.Println()
	//loop patern 2
	isi = 1
	for j := 1; j <= nilai; j++ {
		if j%3 == 0 {
			fmt.Print("#", " ")
			isi++
		} else {
			fmt.Print(isi, " ")
			isi++
		}
	}
}
