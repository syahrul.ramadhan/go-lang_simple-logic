package main

import "fmt"

func main() {
	var jenis string
	var deret int
	var ganjil = 1
	var genap = 2

	fmt.Print("panjang deret :")
	fmt.Scanln(&deret)
	fmt.Print("jenis :")
	fmt.Scanln(&jenis)

	// if jenis == "genap" {
	// 	for i := 0; i < deret; i++ {
	// 		fmt.Print(genap, " ")
	// 		genap += 2
	// 	}
	// } else {
	// 	for i := 0; i < deret; i++ {
	// 		fmt.Print(ganjil, " ")
	// 		ganjil += 2
	// 	}
	// }

	switch jenis {
	case "genap":
		for i := 0; i < deret; i++ {
			fmt.Print(genap, " ")
			genap += 2
		}
	case "ganjil":
		for i := 0; i < deret; i++ {
			fmt.Print(ganjil, " ")
			ganjil += 2
		}
	}
}
