package main

import (
	"fmt"
	"strings"
)

func main() {
	var alpa = "abcdefghijklmnopqrstuvwxyz"
	fmt.Println(strings.Contains("test", "es"))
	fmt.Println(strings.Count("BANANA", "NA"))
	fmt.Println(strings.HasPrefix("test", "te"))
	fmt.Println(strings.HasSuffix("test", "st"))
	fmt.Println(strings.Index("thequickbrownfoxjumpsoverthelazy", "abcdefghijklmnopqrstuvwxyz"))
	fmt.Println(strings.Join([]string{"t", "e"}, ""))
	fmt.Println(strings.Repeat("test", 9))
	fmt.Println(strings.Replace("teste", "e", "o", 10))
	fmt.Println(strings.Split("test", ""))
	fmt.Println(strings.ToLower("Yes"))
	fmt.Println(strings.ToUpper("syahrul"))
	fmt.Println(strings.EqualFold("syahrul", "SYAHRUL"))

	for _, v := range alpa {
		fmt.Println(strings.IndexRune("thequickbrownfoxjumpsoverthelazy", v))

	}
}
