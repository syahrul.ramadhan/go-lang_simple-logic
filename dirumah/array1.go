package main

import (
	"fmt"
)

func main() {
	var menu = []string{"ikan", "ayam"}
	var isi string
	var pilih string
	var ulang bool = false

	for ulang == false {
		fmt.Print("masukkan data :")
		fmt.Scanln(&isi)
		menu = append(menu, isi)
		fmt.Print("ingin memasukan data lagi (yes/no) ? ")
		fmt.Scanln(&pilih)
		if pilih == "no" {
			ulang = true
		}
	}
	fmt.Println("semua data yang telah masuk :")
	for i := 0; i < len(menu); i++ {
		fmt.Print(i, ".", menu[i], "\n")
	}

}
