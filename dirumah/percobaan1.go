package main

import "fmt"

func main() {
	var nilai = []int{4, 5, 6, 1, 2, 3, 7, 8, 9}
	var wadah int

	for i := len(nilai); i > 0; i-- {
		for j := 1; j < i; j++ {
			if nilai[j-1] < nilai[j] {
				wadah = nilai[j]
				nilai[j] = nilai[j-1]
				nilai[j-1] = wadah
			}
		}
	}

	for i := 0; i < len(nilai); i++ {
		fmt.Print(nilai[i], " ")
	}
}
