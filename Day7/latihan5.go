package main

import "fmt"

func main() {
	//perulangan dengan menampilkan bintang yang berurut ke bawah dan memiliki spasi
	fmt.Println("kebawah")
	var bintang = "*"
	for i := 0; i < 9; i++ {
		fmt.Println()
		for j := 0; j < 9; j++ {
			if j%2 == 0 {
				fmt.Print(bintang)
			} else {
				fmt.Print("  ")
			}
		}
	}
	//perulangan dengan menampilkan bintang yang berurut ke samping dan memiliki spasi
	fmt.Println()
	fmt.Println("kesamping")
	for i := 0; i < 9; i++ {
		fmt.Println()
		for j := 0; j < 9; j++ {
			if i%2 == 0 {
				fmt.Print(" ")
			} else {
				fmt.Print(bintang)
			}
		}
	}
}
