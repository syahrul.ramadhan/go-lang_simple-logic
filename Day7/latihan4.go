package main

import "fmt"

func main() {
	nilai := 1
	for i := 1; i <= 9; i++ {
		for j := 1; j <= 9; j++ {
			fmt.Print(nilai, " ")
			nilai += 2
		}
	}
}
