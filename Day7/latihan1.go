package main

import "fmt"

func main() {
	var pilih int
	var ganti string
	var jMakanan string
	for {
		fmt.Println("Halo kaka, dipilih menunya?")
		fmt.Println("1. Ayam")
		fmt.Println("2. Ikan")
		fmt.Println("3. Sapi")
		fmt.Print("Pilih no? ")
		fmt.Scanln(&pilih)
		if pilih == 1 {
			jMakanan = "Ayam"
		} else if pilih == 2 {
			jMakanan = "Ikan"
		} else if pilih == 3 {
			jMakanan = "Sapi"
		} else {
			fmt.Println("Salah menu")
			continue
		}
		fmt.Print("ganti pilihan (yes/no) ? ")
		fmt.Scanln(&ganti)
		if ganti == "no" {
			break
		}
	}

	fmt.Println("kaka telah memilih", jMakanan)
	fmt.Println("terima kasih telah berbelanja ")
}
