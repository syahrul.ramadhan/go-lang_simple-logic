package main

import (
	"fmt"
)

func main() {
	//posisi minus
	fmt.Println("Posisi Minus :")
	var minus2 int = 1
	for i := 1; i <= 6; i++ {
		if i%2 == 0 {
			fmt.Print(-minus2, " ")
		} else {
			fmt.Print(minus2, " ")
		}
		minus2 += 2
	}
	fmt.Println()
	var minus3 int = 1
	for i := 1; i <= 6; i++ {
		if i%3 == 0 {
			fmt.Print(-minus3, " ")
		} else {
			fmt.Print(minus3, " ")
		}
		minus3 += 2
	}
	fmt.Println()
	fmt.Println("Posisi dinamis :")
	//posisi dinamis
	var dinamis1 = 1
	for i := 1; i <= 6; i++ {
		if i%3 == 0 {
			fmt.Print("999", " ")
		} else {
			fmt.Print(dinamis1, " ")
		}
		dinamis1 += 2
	}
	fmt.Println()
	var dinamis2 = 1
	for i := 1; i <= 6; i++ {
		if i%3 == 0 {
			fmt.Print("999", " ")
		} else {
			fmt.Print(dinamis2, " ")
			dinamis2 += 2

		}
	}
	fmt.Println()
	var dinamis3 = 1
	for i := 1; i <= 6; i++ {
		if i%3 == 0 {
			fmt.Print("999", " ")
		} else if dinamis3%3 == 0 {
			fmt.Print(-dinamis3, " ")
		} else {
			fmt.Print(dinamis3, " ")
		}
		dinamis3 += 2
	}
	fmt.Println()
	var dinamis4 = 1
	for i := 1; i <= 9; i++ {
		if i%3 == 0 {
			fmt.Print("X", " ")
		} else if dinamis4%3 == 0 {
			fmt.Print(-dinamis4, " ")
		} else {
			fmt.Print(dinamis4, " ")
		}
		dinamis4 += 2
	}

	fmt.Println()
	var dinamis5 = 1
	var asc int = 65
	for i := 1; i <= 9; i++ {
		if i%3 == 0 {
			fmt.Printf("%c ", asc)
			asc++
		} else if dinamis5%3 == 0 {
			fmt.Print(-dinamis5, " ")
		} else {
			fmt.Print(dinamis5, " ")
		}
		dinamis5 += 2
	}

}
