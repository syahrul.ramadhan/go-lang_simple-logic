package main

import (
	"fmt"
)

func main() {
	//bilangan cacah
	fmt.Println("bilangan cacah :")
	for i := 0; i < 7; i++ {
		fmt.Print(i, " ")
	}
	fmt.Println()
	//bilangan asli
	fmt.Println("bilangan asli :")
	for i := 1; i <= 7; i++ {
		fmt.Print(i, " ")
	}
	fmt.Println()
	//bilangan ganjil
	fmt.Println("bilangan ganjil :")
	var ganjil = 1
	for i := 1; i <= 7; i++ {
		fmt.Print(ganjil, " ")
		ganjil += 2
	}
	fmt.Println()
	//bilangan genap
	fmt.Println("bilangan genap:")
	var genap = 2
	for i := 0; i < 7; i++ {
		fmt.Print(genap, " ")
		genap += 2
	}
	fmt.Println()
	//bilangan kelipatan
	fmt.Println("bilangan kelipatan :")
	var kelipatan = 5
	for i := 0; i < 7; i++ {
		fmt.Print(kelipatan, " ")
		kelipatan += 5
	}
	fmt.Println()
	//bilangan fibonacci
	fmt.Println("bilangan fibonacci :")
	var fibo, a, b = 0, 1, 1
	for i := 0; i < 7; i++ {
		fmt.Print(a, " ")
		fibo = a + b
		a = b
		b = fibo
	}
	fmt.Println()
	//bilangan tribonachi
	fmt.Println("bilangan tribonacci :")
	var tri, d, e, f = 0, 1, 1, 1
	for i := 0; i < 7; i++ {
		fmt.Print(d, " ")
		tri = d + e + f
		d = e
		e = f
		f = tri
	}
	fmt.Println()
	//bilangan pangkat
	fmt.Println("bilangan pangkat :")
	var pangkat = 2
	for i := 0; i < 7; i++ {
		fmt.Print(pangkat, " ")
		pangkat *= 2
	}
	fmt.Println()
	//bilangan alphabet
	fmt.Println("bilangan alphabet :")
	for i := 97; i <= 122; i++ {
		fmt.Printf("%c ", i)
	}
}
