package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	baca := bufio.NewReader(os.Stdin)
	var nama string
	var aVocal []string
	var aKonsonan []string
	var tampung string
	var tampung2 string
	fmt.Print("Input one line of words (S): ")
	in1, _ := baca.ReadString('\n')
	nama = in1
	nama = strings.TrimSuffix(nama, "\r\n")
	nama = strings.ToLower(nama)
	nama = strings.ReplaceAll(nama, " ", "")
	arr := strings.Split(nama, "")

	for i := 0; i < len(arr); i++ {
		if arr[i] == "a" || arr[i] == "i" || arr[i] == "u" || arr[i] == "e" || arr[i] == "o" {
			aVocal = append(aVocal, arr[i])
		} else {
			aKonsonan = append(aKonsonan, arr[i])
		}
	}

	for i := 0; i < len(aVocal); i++ {
		for j := 0; j < len(aVocal)-1; j++ {
			if aVocal[j] != aVocal[j+1] {
				tampung = aVocal[j+1]
				aVocal[j+1] = aVocal[j]
				aVocal[j] = tampung
			} else {
				break
				
			}
		}
	}

	for i := 0; i < len(aKonsonan); i++ {
		for j := 0; j < len(aKonsonan)-1; j++ {
			if aKonsonan[j] != aKonsonan[j+1] {
				tampung2 = aKonsonan[j+1]
				aKonsonan[j+1] = aKonsonan[j]
				aKonsonan[j] = tampung2
			} else {
				break
				
			}
		}
	}

	fmt.Print("Vowel Characters :")
	for i := 0; i < len(aVocal); i++ {
		fmt.Print(aVocal[i], "")
	}
	fmt.Println()
	fmt.Print("Consonant Characters : ")
	for i := 0; i < len(aKonsonan); i++ {
		fmt.Print(aKonsonan[i], "")
	}
}