package main

import "fmt"

func main() {
	var panjang int
	var lebar int
	var char string

	fmt.Print("panjang :")
	fmt.Scanln(&panjang)
	fmt.Print("lebar :")
	fmt.Scanln(&lebar)
	fmt.Print("karakter :")
	fmt.Scanln(&char)

	for i := 1; i <= lebar; i++ {
		for j := 0; j < panjang; j++ {
			fmt.Print(char, " ")
		}
		panjang--
		fmt.Println()
	}
}
