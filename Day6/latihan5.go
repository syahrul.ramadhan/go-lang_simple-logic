package main

import (
	"fmt"
	ma "math/rand"
	"time"
)

func main() {
	max, min := 10, 1
	var random, input int
	var ulang string
	var life = 10

	for {
		fmt.Println("nyawa anda :", life)
		fmt.Print("tebak angka : ")
		fmt.Scanln(&input)
		if input < min && input > max {
			fmt.Println("angka tebakan mu melebihi batas")
		} else {
			ma.Seed(time.Now().UnixNano())
			random = ma.Intn(max-min) + min

			fmt.Println("angka Cpu :", random)

			if random == input {
				fmt.Println("Player Menang")
				life++
			} else {
				fmt.Println("Cpu Menang")
				life--
			}
		}
		fmt.Print("ulang ? (yes/no) ")
		fmt.Scanln(&ulang)
		if ulang == "no" {
			fmt.Println("Permainan selesai")
			break
		}

	}

}
