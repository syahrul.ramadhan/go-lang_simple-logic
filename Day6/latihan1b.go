package main

import "fmt"

func main() {
	var panjang int
	var lebar int
	var char string

	fmt.Print("panjang :")
	fmt.Scanln(&panjang)
	fmt.Print("lebar :")
	fmt.Scanln(&lebar)
	fmt.Print("karakter :")
	fmt.Scanln(&char)

	for i := 1; i <= lebar; i++ {
		fmt.Println()
		for j := 1; j <= i; j++ {
			fmt.Print(char, " ")
		}
	}
}
