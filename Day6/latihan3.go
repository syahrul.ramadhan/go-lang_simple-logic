package main

import "fmt"

func main() {

	var jeda int

	fmt.Print("berhenti di ?")
	fmt.Scanln(&jeda)

	for i := 1; i <= 10; i++ {
		fmt.Print(i, " ")
		if i == jeda {
			fmt.Println()
			fmt.Print("Break")
			break
		}
	}
	fmt.Println()
	for i := 1; ; i++ {
		fmt.Print(i, " ")
		if i == jeda {
			fmt.Println()
			fmt.Print("Break")
			break
		}
	}
}
