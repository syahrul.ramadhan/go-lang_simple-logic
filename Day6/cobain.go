package main

import (
	"fmt"
	ma "math/rand"
	"time"
)

func main() {
	max, min := 10, 1
	var random, input int
	var ulang string
	var life int
	var taruhan int
	var uangCpu int

	fmt.Print("uang awal ?")
	fmt.Scanln(&life)

	fmt.Print("uang cpu ?")
	fmt.Scanln(&uangCpu)

	for {
		fmt.Print("pasang taruhan ?")
		fmt.Scanln(&taruhan)
		fmt.Print("tebak angka : ")
		fmt.Scanln(&input)
		if input < min && input > max {
			fmt.Println("angka tebakan mu melebihi batas")
		} else {
			ma.Seed(time.Now().UnixNano())
			random = ma.Intn(max-min) + min

			fmt.Println("angka Cpu :", random)

			if random == input {
				fmt.Println("Player Menang")
				life += taruhan
				fmt.Println("uang anda :", life)
			} else {
				fmt.Println("Cpu Menang")
				life -= taruhan
				fmt.Println("uang anda :", life)
			}
		}

		if life <= 0 {
			fmt.Println("uang anda habis, rungkat dulu guys")
			fmt.Println("silahkan depo lagi")
			break
		} else {
			fmt.Print("ulang ? (yes/no) ")
			fmt.Scanln(&ulang)
			if ulang == "no" {
				fmt.Println("Permainan selesai")
				fmt.Println("uang terkumpul", life)
				break
			}
		}
	}

}
