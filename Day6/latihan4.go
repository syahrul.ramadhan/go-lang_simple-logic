package main

import "fmt"

func main() {

	var pilih int
	//var exit = false

	//cara pribadi
	for {
		fmt.Println("==Sistem akademik==")
		fmt.Println("1. Input data")
		fmt.Println("2. Exit")
		fmt.Print("Pilih ? ")
		fmt.Scanln(&pilih)
		if pilih == 2 {
			fmt.Println("exit")
			fmt.Println("Terimakasih")
			break
		}
	}

	//cara trainer
	// for exit == false {
	// 	fmt.Println("==Sistem akademik==")
	// 	fmt.Println("1. Input data")
	// 	fmt.Println("2. Exit")
	// 	fmt.Print("Pilih ? ")
	// 	fmt.Scanln(&pilih)
	// 	if pilih == 2 {
	// 		exit = true
	// 		fmt.Println("exit")
	// 		fmt.Println("Terimakasih")
	// 	}
	// }

}
