package main

import "fmt"

func main() {
	//progmram perulangan dimana mengulang sesuai dengan jumlah yang inputkan dengan panjang dan lebar atau dua looping
	var panjang int
	var lebar int
	var char string = "@"

	fmt.Print("panjang :")
	fmt.Scanln(&panjang)
	fmt.Print("lebar :")
	fmt.Scanln(&lebar)
	fmt.Print("karakter :")
	fmt.Scanln(&char)

	for i := 0; i < lebar; i++ {
		fmt.Println(" ")
		for i := 0; i < panjang; i++ {
			fmt.Print(char, " ")
		}
	}
}
